const _ = require("lodash");
const convert = require("./convert");
const { latToAngle, lonToAngle, bilinearInterpolation } = require("./utils");

class GribLayer {
  constructor({ header, data }, strict = true) {
    Object.assign(this, header, { data });
    this.$strict = strict;
    this.id = _.camelCase(header.parameterNumberName);

    this.dx = Math.abs(this.lo1 - this.lo2) / (this.nx - 1);
    this.dy = Math.abs(this.la1 - this.la2) / (this.ny - 1);
  }
  lonToX(lon) {
    let angle = lonToAngle(lon) - lonToAngle(this.lo1);
    return angle / this.dx;
  }
  latToY(lat) {
    let angle = latToAngle(lat) - latToAngle(this.la1);
    return angle / this.dy;
  }
  get(lat, lon) {
    let x = this.lonToX(lon),
      y = this.latToY(lat);
    let x0 = Math.floor(x),
      y0 = Math.floor(y);
    let [a, b, c, d] = [
      [x0, y0],
      [x0 + 1, y0],
      [x0, y0 + 1],
      [x0 + 1, y0 + 1]
    ].map(function([x, y]) {
      let value = this.getXY(x, y);
      return { x, y, value };
    }, this);
    let value = bilinearInterpolation([a, b, c, d], x, y);
    return value;
  }
  getXY(x, y) {
    if (!this.$strict) {
      [x, y] = this.normalizeXY(x, y);
    }
    let key = this.nx * y + x;
    return this.data[key];
  }
  normalizeXY(x, y) {
    x %= this.nx;
    if (x < 0) x += this.nx;
    y %= 2 * this.ny;
    if (y < 0) y = -y - 1;
    if (y > this.ny) y = this.ny - (y - this.ny - 1);
    return [x, y];
  }
}

class Grib {
  constructor(config) {
    Object.assign(this, { strict: true }, config);
    this.layers = [];
  }
  addLayer(layer) {
    this.layers.push(layer);
  }
  get(lat, lon) {
    let res = {};
    this.layers.forEach(layer => {
      let id = layer.id;
      let value = layer.get(lat, lon);
      res[id] = value;
    });
    return res;
  }
  async loadFile(src) {
    let layers = await convert(src);
    layers = layers.map(layer => new GribLayer(layer, this.strict));
    layers.forEach(function(layer) {
      this.addLayer(layer);
    }, this);
  }
}

module.exports = Grib;
