const path = require("path");
const Grib = require("../index.js");

async function test() {
  let grib = new Grib({ strict: false });
  await grib.loadFile(path.resolve(__dirname, "data", "2020-01-20T00.grib2"));
  console.log(grib.get(43, -0.01));
  console.log(grib.get(90, 90));
}

test();
