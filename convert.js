const path = require("path");
const grib2json = require("grib2json").default;

module.exports = function convert(src, config = {}) {
  return new Promise(function(resolve, reject) {
    try {
      grib2json(
        src,
        {
          scriptPath: path.resolve(__dirname, "./lib/grib2json/bin/grib2json"),
          names: true, // (default false): Return descriptive names too
          data: true, // (default false): Return data, not just headers
          ...config
        },
        function(err, json) {
          if (err) throw new Error(err);
          resolve(json);
        }
      );
    } catch (e) {
      reject(e);
    }
  });
};
