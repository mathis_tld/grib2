exports.barycentre = function barycentre(val1, val2, cursor) {
  return (1 - cursor) * val1 + cursor * val2;
};

exports.bilinearInterpolation = function bilinearInterpolation(
  [a, b, c, d],
  x,
  y
) {
  // https://fr.wikipedia.org/wiki/Interpolation_bilin%C3%A9aire
  // assumes grid is as following
  // a b
  // c d
  // a,b,c,d must have x,y,value properties
  let dx = x - a.x,
    dy = y - a.y,
    diffx = b.x - a.x,
    diffy = c.y - a.y;
  let value =
    (b.value - a.value) * (dx / diffx) +
    (c.value - a.value) * (dy / diffy) +
    (a.value + d.value - b.value - c.value) * (dx / diffx) * (dy / diffy) +
    a.value;
  return value;
};

exports.sign = function sign(x) {
  return x < 0 ? "-" : "+";
};
