const utils = {
  ...require("./maths"),
  ...require("./geo")
};
module.exports = utils;
