const EARTH_RADIUS = 6371000;
exports.EARTH_RADIUS = EARTH_RADIUS;

exports.distance = function distance(loc1, loc2) {
  let { asin, sin, cos, sqrt, pow } = Math;
  let lat1 = loc1.lat * (Math.PI / 180);
  let lat2 = loc2.lat * (Math.PI / 180);
  let lon1 = loc1.lon * (Math.PI / 180);
  let lon2 = loc2.lon * (Math.PI / 180);
  // https://fr.wikipedia.org/wiki/Formule_de_haversine
  let d =
    2 *
    EARTH_RADIUS *
    asin(
      sqrt(
        pow(sin((lat2 - lat1) / 2), 2) +
          cos(lat1) * cos(lat2) * pow(sin((lon2 - lon1) / 2), 2)
      )
    );
  return d;
};

function normalizeLon(lon) {
  let absLon = Math.abs(lon) % 360;
  let sign = lon < 0 ? -1 : 1;
  if (absLon > 180) {
    absLon = Math.abs(360 - absLon);
    return -1 * sign * absLon;
  }
  return sign * absLon;
}
function normalizeLat(lat) {
  let absLat = Math.abs(lat) % 360;
  let sign = lat < 0 ? -1 : 1;
  if (absLat > 180) {
    return 180;
  }
  if (90 < absLat && absLat <= 180) {
    return sign * (180 - absLat);
  } else if (180 < absLat && absLat <= 270) {
    return sign * (270 - absLat);
  } else if (180 < absLat && absLat <= 270) {
    return sign * (270 - absLat);
  }
  return lat;
}

exports.latToAngle = function latToAngle(lat) {
  return 90 - lat;
};
exports.angleToLat = function angleToLat(angle) {
  return 90 - angle;
};
exports.lonToAngle = function lonToAngle(lon) {
  if (lon >= 0) {
    return lon;
  } else {
    return 360 + lon;
  }
};
exports.angleToLon = function angleToLon(angle) {
  if (angle <= 180) {
    return angle;
  } else {
    return angle - 360;
  }
};
